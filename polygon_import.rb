require 'net/http'
module Net
  class HTTP
    alias old_initialize initialize

    # just monkey patch the connection timeout, because I'm too lazy to figure out proper way to set it 
    # (can't set it when using Net::HTTP.get_response(uri), so...)
    def initialize(*args)
      old_initialize(*args)
      @read_timeout = 120     # 2 minutes
    end
  end
end

module PolygonImport
  require 'readline'
  require 'fileutils'
  require 'json'

  PKG_FOLDER = 'downloadedPackages'

  def self.buildParams(apiMethod, unfinishedParams)
    begin
      keyfile = File.new('polygon_API.key')
    rescue Exception
      abort('No key file found. Make sure file `polygon_API.key` exists and has the API key on 1st line and API secret on 2nd line')
    end

    key = keyfile.gets.chomp
    secret = keyfile.gets.chomp
    unfinishedParams['apiKey'] = key
    unfinishedParams['time'] = Time.now.to_i

    # TODO: I only sort by params here, not by values, but I don't think this is an issue... See https://docs.google.com/document/d/1mb6CDWpbLQsi7F5UjAdwXdbCpyvSgWSXTJVHl52zZUQ/edit#
    p unfinishedParams
    paramString = unfinishedParams.sort.to_a.map {|el| "#{el[0]}=#{el[1]}"}
    paramString = paramString.join("&")
    rand = "abcdef" # chosen by a fair dice roll
    secretString = "#{rand}/#{apiMethod}?#{paramString}##{secret}"
    hashedSecret = %x"echo -n '#{secretString}' | sha512sum | cut -d ' ' -f 1".chomp
    unfinishedParams['apiSig'] = "#{rand}#{hashedSecret}"
    #puts "Request params: #{unfinishedParams}"
    return unfinishedParams
  end

  def self.buildURI(apiMethod, unfinishedParams)
    params = self.buildParams(apiMethod, unfinishedParams)
    uri = URI("https://polygon.codeforces.com/api/#{apiMethod}")
    uri.query = URI.encode_www_form(params)
    uri
  end

  def self.getJson(apiMethod, unfinishedParams)
    uri = buildURI(apiMethod, unfinishedParams)

    begin
      res = Net::HTTP.get_response(uri)
      if not res.is_a?(Net::HTTPSuccess) then
        raise Exception
      end
      # request = RestClient.get "https://polygon.codeforces.com/api/#{apiMethod}", {params: params}
    rescue Exception => e
      abort("Failed request to polygon server. Error message: #{res.body.response}")
    end

    json = JSON.parse(res.body)
    if json['status'] != "OK" then
      abort("Polygon server returned unsuccessful json: #{json}")
    end
    return json['result']
  end

  def self.saveZip(problemId, packageId)
    uri = buildURI('problem.package', {'problemId' => problemId, 'packageId' => packageId})

    FileUtils.mkdir_p(PKG_FOLDER)
    zipPath = File.join(PKG_FOLDER, "package_#{packageId}.zip")

    # gotta do this complicated setup cuz we zipfiles are up to ~200MB
    # which is uncomfortable without streaming.
    Net::HTTP.start(uri.host, uri.port, :use_ssl => true) do |http|
      request = Net::HTTP::Get.new uri

      http.request(request) do |response|
        File.open(zipPath, 'w') do |f|
          response.read_body do |chunk|
            f.write(chunk)
          end
        end
      end
    end

    puts "Download succeeded"
  end

  # So far, the only time I didn't get a ready package was with my test problems where I hadn't added anything
  # so I don't think it ever happens that the first package in a list is non-ready, but better safe than sorry.
  def self.findReadyPackage(packageJsonList)
    ready_package = nil
    packageJsonList.each do |package|
      if package['state'] != "READY" then
        next
      else
        ready_package = package
      end
    end
    return ready_package
  end


  def self.fetchProblems()
    puts "Listing problems..."
    problems = getJson('contest.problems', {"contestId"=>16049})
    puts "Got problems #{problems}."

    problems.each do |problem, content|
      puts
      puts
      puts
      id = content['id']
      puts "Listing packages of problem #{id}..."
      packages = getJson('problem.packages', {"problemId"=>id})
      puts "Got packages #{packages}."

      puts "Looking for package that is 'READY'..."
      package = findReadyPackage(packages)
      if package == nil then
        abort('No package in "READY" state found. I don\'t know why this happened, but maybe try creating a package via the polygon website.')
      end
      packageId = package['id']
      puts "Found package: Downloading package #{packageId} of problem #{id}"
      saveZip(id, packageId)
      puts "Done!"
    end
  end
end

PolygonImport.fetchProblems()
