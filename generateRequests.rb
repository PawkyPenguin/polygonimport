#!/usr/bin/env ruby

require 'rest-client'
require 'readline'

key = ""
secret = ""
keyfile = File.new('polygon_API.key')
key = keyfile.gets.chomp
secret = keyfile.gets.chomp
puts "key: #{key}"
puts "secret: #{secret}"

# stdin.gets.chomp
while true do
  puts "_______________________________________________"
  puts "API method?"
  method = STDIN.gets.chomp
  puts "params? Enter one per line, end with Ctrl+D"
  params = {}

  while param = Readline.readline("", true)
    k, v = param.chomp.split(" ")
    params[k] = v
  end
  params['apiKey'] = key
  params['time'] = Time.now.to_i

  # TODO: I only sort by params here, not by values, but I don't think this is an issue... See https://docs.google.com/document/d/1mb6CDWpbLQsi7F5UjAdwXdbCpyvSgWSXTJVHl52zZUQ/edit#
  paramString = params.sort.to_a.map {|el| "#{el[0]}=#{el[1]}"}
  paramString = paramString.join("&")
  rand = "abcdef"
  secretString = "#{rand}/#{method}?#{paramString}##{secret}"
  hashedSecret = %x"echo -n '#{secretString}' | sha512sum | cut -d ' ' -f 1".chomp
  params['apiSig'] = "#{rand}#{hashedSecret}"
  puts "Secret-string: #{secretString}"
  puts "Hashed string: #{params['apiSig']}"
  puts "Params: #{params}"

  begin
     request = RestClient.get "https://polygon.codeforces.com/api/#{method}", {params: params}
     puts "Answer: "
     puts "#{request.body}"
   rescue RestClient::ExceptionWithResponse => e
     puts e.response
   end
end
