#!/usr/bin/env ruby

require 'fileutils'
require 'rexml/document'
#require 'shell'

def compile(file)
  if not File.exists?(file) then
    warn("no file '#{file}'")
    return
  end
  if not system('g++', '-I', 'files/', '-o', file + '.exe', file) then
    abort("Error: Compilation of file '#{file}' failed.")
  end
end

def getSolution
  File.open('problem.xml', 'r') do |f|
    xml = REXML::Document.new(f)
    solution = REXML::XPath.first(xml, "//solution[@tag='main']/source[@path]").attributes['path']
    puts("Solution: #{solution}")
    return solution.to_s
  end
end

def compileProblemFiles
  toCompile = ['files/check.cpp', 'files/validator.cpp', getSolution]
  toCompile.each do |filename|
    compile(filename)
  end
end

def testFiles
  tests =  Dir.glob('tests/*').filter {|f| not f.end_with?('.a')}
  p tests
  return tests
end

Dir.glob('*.zip').each do |zipfile|
  unzippedName = zipfile[..-5]
  if not system('/usr/bin/env', 'unzip', '-o', '-d', unzippedName, zipfile) then
    abort("Error: couldn't execute unzip")
  end

  FileUtils.cd(unzippedName)
  compileProblemFiles()
  testFiles().each do |test|

    # TODO: this is really only for "security", because otherwise we can get code injection
    # in the shell commands below. Might fix once I figure out how to piping & redirection properly.
    if not test.match?(/^[a-zA-Z0-9.-_\/]*$/) then
      abort("Test '#{test}' does not use reasonable charset, aborting execution")
    end
    if File.exists?(test + '.a') then
      puts("File '#{test}.a' already exists; not regenerating.")
      next
    end

    testOutfile = test + '.a'
    if File.exists?('./files/validator.cpp.exe') then
      puts("Executing validator for '#{unzippedName}/#{test}'")
      `./files/validator.cpp.exe --testset tests --group "" < "#{test}"`
      if not $?.sucess? then
        # I'm not aborting here because validating is optional and even the example contest had a failing validator...
        warn("WARNING: Validator did not succeed")
      end
    else
      puts('Skipping validator because not found')
    end
    puts("Generating test output for '#{unzippedName}/#{test}'")
    `./#{getSolution}.exe < "#{test}" > "#{testOutfile}"`
    if not $?.success? then
      abort("Error: Solution returned non-zero exit code")
    end
    if not File.exists?(testOutfile) then
      abort("Error: Solution didn't produce output")
    end

    # don't ask me why I need the same argument twice...
    puts("Executing checker for test '#{unzippedName}/#{test}'...")
    system('./files/check.cpp.exe', test, testOutfile, testOutfile)
    if not $?.success? and $?.exitstatus != 7 then
      # apparently 7 is also, not sure why
      abort("Error: Checker exit code is not equal to 0 or 7")
    end
  end

  FileUtils.cd('..')
end
